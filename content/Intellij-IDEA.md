# Intellij IDEA的激活（使用破解补丁永久激活）  
## 下载并安装IDEA
[官网](https://www.jetbrains.com/idea/)  https://www.jetbrains.com/idea/  
## 下载破解补丁
 [下载地址](http://idea.lanyus.com/jar)  
1. 将下载的文件放到安装IDEA路径的bin目录下（如图）
<div align="center"> <img src="../image/20180318124712844.jpg" width="400"/> </div><br>
2. 修改配置文件
    找到  idea.exe.vmoptions  和  idea64.exe.vmoptions  ，用文本工具或记事本打开它们,在两个文件最后分别加上（如下代码）

``` HTML
-javaagent:C:\Program Files\JetBrains\IntelliJ IDEA 2018.2.3\bin\JetbrainsIdesCrack.jar
```

<div align="center"> <img src="../image/20180318125224129.png" width="400"/> </div><br> 
<div align="center"> <img src="../image/20180318125151169.png" width="400"/> </div><br>  

 **注意:**  要修改JetbrainsCrack的路径。  
3. 重启Intellij IDEA，在Activation Code输入  


<div align="center"> <img src="../image/20180318125832208.png" width="400"/> </div><br>

```TXT
ThisCrackLicenseId-{
"licenseId":"ThisCrackLicenseId",
"licenseeName":"你想要的用户名",
"assigneeName":"",
"assigneeEmail":"随便填一个邮箱(我填的:idea@163.com)",
"licenseRestriction":"For This Crack, Only Test! Please support genuine!!!",
"checkConcurrentUse":false,
"products":[
{"code":"II","paidUpTo":"2099-12-31"},
{"code":"DM","paidUpTo":"2099-12-31"},
{"code":"AC","paidUpTo":"2099-12-31"},
{"code":"RS0","paidUpTo":"2099-12-31"},
{"code":"WS","paidUpTo":"2099-12-31"},
{"code":"DPN","paidUpTo":"2099-12-31"},
{"code":"RC","paidUpTo":"2099-12-31"},
{"code":"PS","paidUpTo":"2099-12-31"},
{"code":"DC","paidUpTo":"2099-12-31"},
{"code":"RM","paidUpTo":"2099-12-31"},
{"code":"CL","paidUpTo":"2099-12-31"},
{"code":"PC","paidUpTo":"2099-12-31"}
],
"hash":"2911276/0",
"gracePeriodDays":7,
"autoProlongated":false}
```

4. 然后……就成功了到2100年。
<div align="center"> <img src="../image/20180318125835265.png" width="400"/> </div><br>
